# 软件工程专业教育

- 软件工程2019-1-2班，星期六(11月14日)  8:30-17:30  10502
- 软件工程2019-3-4班，星期天(11月22日)  8:30-17:30  10502，AWS Academy Cloud Foundations [1846]
- 软件工程2018-1-2班，星期天(11月29日)  8:30-17:30  10502，AWS Academy Cloud Foundations [1929]
- 软件工程2018-3-4班，星期天(11月15日)  8:30-17:30  10502

## 时间安排

- 上午：
  - 专业教育
  - AWS云计算介绍
  - 老师项目介绍 (https://www.xionggu.com/)，[项目需求分析](http://www.xionggudata.com:9109/ms10000/analysis/public/ms10000/main/start.html#id=22f3x4&p=index)
- 下午：
  - 个人职业规划
  - 报告撰写
    - 上交打印搞
    - 上交电子档：学号-姓名.docx,按班级压缩
    - 2020年12月1日(星期二)上午34节课交到10教5楼深度学习办公室

## AWS

- [AWS课程登录](https://www.awsacademy.com/LMS_Login)
- [星期日网上课程注册](https://attendee.gototraining.com/r/1751870979417894401)
- [AWS控制台](https://aws.amazon.com/cn/console/)
- [AWS教育培训]https://aws.amazon.com/cn/education/awseducate/
- [AWS全球基础设施](https://infrastructure.aws/)
- [AWS Toolkit for Visual Studio Code](https://github.com/aws/aws-toolkit-vscode)
- AWS在本机的配置文件：~/.aws/credentials
- AWS高可用实验,[WEB地址](http://ec2-35-164-15-22.us-west-2.compute.amazonaws.com/)
- http://labelb-70719196.us-west-2.elb.amazonaws.com/load.php

- 登录EC2服务器,查看web 服务

```shell
$ssh -i "zwd2.pem" ec2-user@ec2-35-164-15-22.us-west-2.compute.amazonaws.com
[ec2-user@ip-10-0-2-41 html]$ cd /var/www/html
[ec2-user@ip-10-0-2-41 html]$ ls
css            get-cpu-load.php         index.php  __MACOSX          rds-config.php  rds-read-data.php     style.css
db-update.php  get-index-meta-data.php  js         menu.php          rds.conf.php    rds-write-config.php
fonts          img                      load.php   put-cpu-load.php  rds.php         sql

```

## 实验1：S3存储桶

[S3实验，访问S3中的上传图片](https://zwd.s3-ap-northeast-1.amazonaws.com/IMG_3145.JPG)
[同学1](https://zwd2.s3-ap-northeast-1.amazonaws.com/dsm.jpg)

```text
- AWS Access key ID：AKIAV7QY6F3GKEUX3LOR
- AWS Secret access key：PybRbQz6NLbiNyLG+GL5t5a6gxSk2pbw0G3+3bEY
```

## [负载测试地址](http://labelb-1933748832.us-west-2.elb.amazonaws.com/load.php)

## 实验2

![实验2](./实验%202%20-%20构建%20VPC%20并启动%20Web%20服务器%20[Lab%202]_files/architecture.png)

## 作为人，何谓正确？稻盛和夫

- 判断标准：按事物本来的是非曲直判断
- 幸福来自于努力奋斗
- 利他
- ...
- [中美事件提醒我们：为什么要感谢特朗普..！（深度好文）](https://mp.weixin.qq.com/s/XrLfv0id8OoywyX4xHwDSw)
- [创业者自述](https://www.ximalaya.com/youshengshu/10816678/58974845)